
tup = (1, 2, 3, 4, 5, 6)
list = []

#conocer el largo de la lista para poder tener perimetros para trabajar 
n = len(tup)

#copiar el ultimo elemento al princiopio
list.append(tup[n-1])

#copiar el resto de los elementos despues del primero
for i in range(0, n-1):
    
    list.append(tup[i])
    
print(tup)
print(list)