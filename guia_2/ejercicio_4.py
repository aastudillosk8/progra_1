
import random

"""
Se crea una lista y se le asignan valores consecutivos
Se inicializa un k con valor random
Se compara el valor de k con la lista para generar 4 listas nuevas 
"""

lista = []
for i in range(1, 26):
    lista.append(i)

k = random.randint(1,26)
print('lista original: ', lista, '\n\nk = ', k)

print()
lista1 = [x for x in lista if x == k]
lista2 = [x for x in lista if x < k]
lista3 = [x for x in lista if x > k]
lista4 = [x for x in lista if x%k == 0]

print('iguales a K: ', lista1)
print()
print('menores que K: ', lista2)
print()
print('mayores que K: ', lista3)
print()
print('multiplos de K: ', lista4)
