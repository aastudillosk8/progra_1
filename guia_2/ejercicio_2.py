
# funcion que crea la matriz
def crear(lista, a, b):

    for i in range(a):
        lista.append([])

        for j in range(b):
            lista[i].append('*')

    return lista


def triangulito(tri, a, b):
    """
    funcion que crea un trianguli que empieza desde las esquinas
    y se cierra en el centro
    condiciones que editan la matriz para dejar el centro vacio
    """
    izq = 0
    der = b - 1
    for i in range(a):
        for j in range(b):
            if j >= izq and j <= der :
                tri[i][j] = ' '
        izq = izq + 1
        der = der - 1
    return tri

# funcion que imprime la matriz
def imprimir(lista) :

    for i in range(altura):
        for j in range(base):

            print(lista[i][j],end=' ')
        print()

# ingreso de variables y llamada a funciones 
altura = int(input('ingrese la altura deseada: '))
base = altura * 2
matriz = []
altura = altura + 1
matriz = crear(matriz, altura, base)

triangulito(matriz, altura, base)
imprimir(matriz)
