# crear una matriz y rellenarla de ceros
def crear(lista, a,):

    for i in range(a):
        lista.append([])
        for j in range(a):
            lista[i].append('0')

    return lista

# imprime una diagonal con el numero 1
def diagonal(matriz, a):
    uno = 0
    for i in range(a):
        for j in range(a):
            if i == uno and j == uno:
                matriz[i][j] = '1'
        uno = uno + 1

    return matriz
# funcion que imprime la matriz
def imprimir(lista, a) :

    for i in range(a):
        for j in range(a):

            print(lista[i][j],end=' ')
        print()
# ingreso de variables y llamada de funciones
cuadrado = int(input('ingrese el valor de su cuadrado: '))
matriz = []
matriz = crear(matriz, cuadrado)
matriz = diagonal(matriz, cuadrado)
imprimir(matriz, cuadrado)
