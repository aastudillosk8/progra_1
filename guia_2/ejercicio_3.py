
# funcion que crea las matrices de cada fijura
def crear(lista, a, b):

    for i in range(a):
        lista.append([])
        for j in range(b):
            lista[i].append('*')

    return lista

def figura_1(tri, a, b):

    izq = a - 1
    der = a
    for i in range(a):
        for j in range(b):
            # base del triangulo
            if i == a - 1 :
                if j >= 1 and j < b - 1:
                    tri[i][j] = '_'
            # lados del triangulo
            if j == izq :
                tri[i][j] = '/'
            if j == der :
                # codigo ascii para imprimir '\'
                tri[i][j] = chr(92)
        izq = izq - 1
        der = der + 1

    return tri

def figura_2(tri, a, b):

    izq = 0
    der = b - 1
    for i in range(a):
        for j in range(b):
            # base deñ triangulo
            if i == 0 :
                if j >= 1 and j < b - 1:
                    tri[i][j] = '-'
            # lados del triangulo
            if j == izq :
                tri[i][j] = chr(92)
            if j == der :
                tri[i][j] = '/'
        izq = izq + 1
        der = der - 1
    return tri

def figura_3(tri, a, b):

    up = 0
    down = a - 1
    for i in range(a):
        for j in range(b):
            if i >= 1 and i <= a -1 :
                if j == 0 :
                    tri[i][j] = '|'
            if j == up :
                tri[i][j] = chr(92)
            if j == down :
                tri[i][j] = '/'
        up = up + 1
        down = down - 1
    return tri

def figura_4(tri, a, b):

    up = b - 1
    down = 0
    for i in range(a):
        for j in range(b):
            if i >= 1 and i <= a - 2  :
                if j == b - 1 :
                    tri[i][j] = '|'
            if j == up :
                tri[i][j] = '/'
            if j == down :
                tri[i][j] = chr(92)
        up = up - 1
        down = down + 1
    return tri

# funcion que imprime las matrices con las figuras
def imprimir(lista, a, b) :

    for i in range(a):
        for j in range(b):

            print(lista[i][j],end=' ')
        print()

# main con ingreso de variables y llamada de funciones
altura = int(input('ingrese la altura deseada: '))
base = altura * 2
normal = []
volteada = []
derecha = []
izquierda = []
normal = crear(normal, altura, base)
volteada = crear(volteada, altura, base)
derecha = crear(derecha, base, altura)
izquierda = crear(izquierda, base, altura)


figura_1(normal, altura, base)
imprimir(normal, altura, base)
print()
figura_2(volteada, altura, base)
imprimir(volteada, altura, base)
print()
figura_3(derecha, base, altura)
imprimir(derecha, base, altura)
print()
figura_4(izquierda, base, altura)
imprimir(izquierda, base, altura)
