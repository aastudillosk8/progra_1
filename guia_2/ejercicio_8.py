import random

# funcion que imprime matrices
def imprimir(list, n):
    for i in range(n):
        for j in range(n):
            print(list[i][j], end='-')
        print()
    print()

def crear(list, n):
    """
    esta funcion crea las matrices y las rellena con
    valores aleatoreso que seran ordenados
    """
    for i in range(n):
        list.append([])
        for j in range(n):
            list[i].append(random.randint(10,99))
    return list

def cambio(matriz, b):

    """
    ordenamiento tipio burbuja, donde se van guardando los
    valores en una matriz temporal para ir ordenandolos
    """
    a = b - 1
    x = int(b/2)
    for j in range(x):
        temp = matriz[a-j][:]
        matriz[a-j][:] = matriz[j][:]
        matriz[j][:] = temp

    return matriz

cuadrado = int(input('ingrese el largo de la matriz: '))
print()
matriz= []
matriz = crear(matriz, cuadrado)
print('original')
imprimir(matriz, cuadrado)
cambio(matriz, cuadrado)
print('ordenada')
imprimir(matriz, cuadrado)
