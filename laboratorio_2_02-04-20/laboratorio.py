#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import json

#abrir el archivo
archivo = open('top50.csv', 'r')

datos = csv.reader(archivo, delimiter=',')

lista_artistas = {}

#pasar todos los datos a variables
for fila in datos:

    posicion = fila[0]
    cancion = fila[1]
    artista = fila[2]
    genero = fila[3]
    #algunos datos es necesario pasarlo a numeros
    beats = float(fila[4])
    energia = fila[5]
    danceability = float(fila[6])
    loudness = float(fila[7])
    liveness =fila[8]
    valence = fila[9]
    duracion = fila[10]
    acousticness = fila[11]
    speechiness = fila[12]
    popularidad = fila[13]

    #pasar datos a diccionarios 
    lista_artistas[artista] += artista

print(lista_artistas)
