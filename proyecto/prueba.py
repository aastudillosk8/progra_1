#!/usr/bin/env python
#! -*- coding: utf-8 -*-
import random

a = []
b = 12
c = 12

# integra las listas en la lista para crear filas y columnas
for i in range(b):
    a.append([])
    for j in range(c):
        r = random.randint(1, 100)
        if r < 80 :
            r = '.'
        elif r >= 80 :
            r = 'X'
        # agrega un . en los espacios vacios
        a[i].append(str(r))
# ciclo que imprime filas y columnas
for i in range(b):
    for j in range(c):
        # "end=" comando para hacer corte de linea
        print(a[i][j],end=' ')
    print()
