

dic = {
    'Histidine': 'C6H9N3O2'
}

#diccionario donde guardaremos el  contenido final
reversa = {}

#entrar a la formula del aminoacido y recorrerla
for aminoacido in dic:

    #formula de la molecula
    formula = dic[aminoacido]

    #variable que donde ira gardandose la molecula
    #que se limpiara cada vez que se recorra el for
    separar = ''


    num = 0

    #sacamos el largo de la formula con len para conocer parametros
    for i in range(len(formula)):

        #le asociamos cada caracter a la variable
        molecula = formula[i]

        #verificar con ".isalpha" si el caracter es una letra
        if molecula.isalpha():

            #si pasamos de un numero a letras es una molecula diferente
            if num == 1:

                reversa[separar] = aminoacido
                num = 0
                separar = ''

            #añadir la letra de la molecula
            separar = separar + molecula
        #verificar con ".isnumeric" si el caracter es un numero
        elif molecula.isnumeric():

            #agregar el numeroa la molecula que separamos
            separar = separar + molecula

            #ver si estamos en un numero
            num = 1

            #condicion para ver que estamos posicionados en el ultimo
            #caracter y que no habran siguientes moleculas
            if i == len(formula) - 1:
                reversa[separar] = aminoacido

print("inicio")
print(dic)
print("velta")
print(reversa)
